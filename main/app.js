
function addTokens(input, tokens){
	if((typeof input) != "string" ) throw new TypeError("Invalid input")
	if(input.length<6) throw new TypeError("Input should have at least 6 characters")
	tokens.forEach(function(token){
			if(typeof token.tokenName !="string") throw new TypeError("Invalid array format")
		})
	if(input.indexOf("...")==-1) return input; //daca nu exista "..."
	else
	{
		for(var i=0;i<tokens.length;i++) //parcurg fiecare element al arrayului
		{
			if(input.indexOf("...")!=-1) //daca mai am "..."
			{
				var rep ="${"+tokens[i].tokenName+"}";
				input= input.replace("...", rep); //le inlocuiesc cu elementul curent 
			}    
		}
		return input; //returnez inputul modificat
	}   
}

const app = {
    addTokens: addTokens
}

module.exports = app;